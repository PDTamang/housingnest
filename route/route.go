package route

import (
	"hn/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func Initialize() {
	router := mux.NewRouter()

	// admin
	router.HandleFunc("/adminSignup", controller.CreateAdminHandler).Methods("POST")
	router.HandleFunc("/adminLogin", controller.LoginAdminHandler).Methods("POST")
	// router.HandleFunc("/logout", controller.LogoutHandler).Methods("GET")

	//owner
	router.HandleFunc("/signup", controller.SignupHandler).Methods("POST")
	router.HandleFunc("/login", controller.LoginHandler).Methods("POST")
	router.HandleFunc("/signup/{email}", controller.GetSignupHandler).Methods("GET")
	router.HandleFunc("/signups", controller.GetAllOwnerHandler).Methods("GET")
	router.HandleFunc("/signup/{email}", controller.DeleteOwnerHandler).Methods("DELETE")
	router.HandleFunc("/user/{email}", controller.UpdateOwenerHandler).Methods("PUT")

	router.HandleFunc("/property", controller.CreatePropertyHandler).Methods("POST")
	// router.HandleFunc("/property/{pid}", controller.GetPropertyHandler).Methods("GET")
	router.HandleFunc("/property/{pid}", controller.UpdatePropertyHandler).Methods("PUT")
	router.HandleFunc("/house", controller.GetAllHouseHandler).Methods("GET")
	router.HandleFunc("/ghouse", controller.GetAllGhouseHandler).Methods("GET")
	router.HandleFunc("/villa", controller.GetAllVillaHandler).Methods("GET")
	router.HandleFunc("/oneproperty/{Pid}", controller.GetPropertyHandler).Methods("GET")
	router.HandleFunc("/ownerPros/{email}", controller.GetPersonalPropertiesHandler).Methods("GET")
	router.HandleFunc("/deletepros/{pid}", controller.DeletePropertyHandler).Methods("DELETE")

	// slr
	router.HandleFunc("/srl", controller.GiveSRHandler).Methods("POST")
	router.HandleFunc("/allsrl", controller.GetAllSRLHandler).Methods("GET")
	router.HandleFunc("/allsrl/{pid}", controller.GetSrlForoneHandler).Methods("GET")

	//websits
	router.HandleFunc("/websit", controller.GiveWebHandler).Methods("POST")
	router.HandleFunc("/allwebsit", controller.GetWebLHandler).Methods("GET")

	// searchengin
	router.HandleFunc("/search/{search}", controller.SearchEngineHandler).Methods("POST")

	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)
	err := http.ListenAndServe(":8084", router)
	if err != nil {
		os.Exit(1)
	}
}
