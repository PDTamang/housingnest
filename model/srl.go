package model

import postgres "hn/datastore"

type SRL struct {
	SRLid  string
	Writer string
	Sgiven int
	Rgiven string
	Pid    string
}

func (srl *SRL) GiveSR() error {
	_, err := postgres.Db.Exec("INSERT INTO slr(SRLid, Writer, Rgiven, Sgiven, Pid) VALUES($1,$2,$3,$4,$5)", srl.SRLid, srl.Writer, srl.Rgiven, srl.Sgiven, srl.Pid)
	return err
}
func GetAllSRL() ([]SRL, error) {
	rows, getErr := postgres.Db.Query("Select * from slr")

	if getErr != nil {
		return nil, getErr
	}

	srl := []SRL{}

	for rows.Next() {
		sr := SRL{}
		dbErr := rows.Scan(&sr.SRLid, &sr.Writer, &sr.Rgiven, &sr.Sgiven, &sr.Pid)

		if dbErr != nil {
			return nil, dbErr
		}
		srl = append(srl, sr)
	}
	rows.Close()
	return srl, nil
}

func (srl *SRL) GetSrlforone() ([]SRL, error) {
	rows, err := postgres.Db.Query("SELECT * FROM slr WHERE Pid = $1", srl.Pid)
	if err != nil {
		return nil, err
	}
	rr := []SRL{}
	for rows.Next() {
		var rs SRL
		dbErr := rows.Scan(&rs.SRLid, &rs.Writer, &rs.Rgiven, &rs.Sgiven, &rs.Pid)
		if dbErr != nil {
			return nil, dbErr
		}
		rr = append(rr, rs)
	}
	rows.Close()
	return rr, nil
}
