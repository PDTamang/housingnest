package model

import (
	"fmt"
	postgres "hn/datastore"
)

type Admin struct {
	AName  string `json:"aname"`
	AEmial string `json:"aemail"`
	Apw    string `json:"apw"`
}

const (
	queryCreateAdmine = "INSERT INTO admins (aname, aemail, apw) values($1,$2, $3);"
	queryCheckAdmin   = "SELECT * FROM admins WHERE aemail = $1;"
)

func (a *Admin) CreateAdmin(s []byte) error {
	fmt.Println(a.AName, "model")
	_, err := postgres.Db.Exec(queryCreateAdmine, a.AName, a.AEmial, s)
	return err
}

func (a *Admin) Check() ([]byte, error) {
	var dbHash []byte
	err := postgres.Db.QueryRow(queryCheckAdmin, a.AEmial).Scan(&a.AName, &a.AEmial, &dbHash)
	return dbHash, err
}
