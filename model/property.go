package model

import postgres "hn/datastore"

type Property struct {
	Pid       string
	Pname     string
	Ptype     string
	Plocation string
	Pdes      string
	Pcost     string
	OE        string
	Pimg      string
}

const (
	queryCreateProperty        = "INSERT INTO property (Pid,Pname,Ptype, Plocation,Pdes, Pcost, OE, Pimg) VALUES($1, $2, $3, $4, $5, $6, $7, $8)"
	queryGetProperty           = "SELECT * FROM property WHERE Pid = $1"
	queryUpdatePropertyDetails = "UPDATE property set Pname=$1, Ptype=$2, Plocation=$3 ,Pdes=$4, Pcost=$5, Pimg = $6 WHERE Pid = $7 RETURNING Pid"
)

func (p *Property) CreateProperty() error {
	_, err := postgres.Db.Exec(queryCreateProperty, p.Pid, p.Pname, p.Ptype, p.Plocation, p.Pdes, p.Pcost, p.OE, p.Pimg)
	return err
}

func (property *Property) GetProperty() error {
	const queryGetUser = "SELECT * FROM property WHERE Pid =$1;"
	return postgres.Db.QueryRow(queryGetUser, property.Pid).Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)
}

func (p *Property) UpdatePropertyDetails(pid string) error {
	err := postgres.Db.QueryRow(queryUpdatePropertyDetails, p.Pname, p.Ptype, p.Plocation, p.Pdes, p.Pcost, p.Pimg, pid).Scan(&p.Pid)
	return err
}

func GetAllHouse() ([]Property, error) {
	rows, getErr := postgres.Db.Query("Select * from property where Ptype = 'House';")

	if getErr != nil {
		return nil, getErr
	}

	properties := []Property{}

	for rows.Next() {
		property := Property{}
		dbErr := rows.Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)

		if dbErr != nil {
			return nil, dbErr
		}
		properties = append(properties, property)
	}
	rows.Close()
	return properties, nil
}

func GetAllGhouse() ([]Property, error) {
	rows, getErr := postgres.Db.Query("Select * from property where Ptype = 'Guest House';")

	if getErr != nil {
		return nil, getErr
	}

	properties := []Property{}
	for rows.Next() {
		property := Property{}
		dbErr := rows.Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)

		if dbErr != nil {
			return nil, dbErr
		}
		properties = append(properties, property)
	}
	rows.Close()
	return properties, nil
}

func GetAllVilla() ([]Property, error) {
	rows, getErr := postgres.Db.Query("Select * from property where Ptype = 'Villa';")

	if getErr != nil {
		return nil, getErr
	}
	properties := []Property{}

	for rows.Next() {
		property := Property{}
		dbErr := rows.Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)
		if dbErr != nil {
			return nil, dbErr
		}
		properties = append(properties, property)
	}
	rows.Close()
	return properties, nil
}

func (p *Property) GetAllPersonalProps() ([]Property, error) {
	rows, err := postgres.Db.Query("SELECT * FROM property WHERE OE = $1", p.OE)
	if err != nil {
		return nil, err
	}
	props := []Property{}
	for rows.Next() {
		var property Property
		dbErr := rows.Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)
		if dbErr != nil {
			return nil, dbErr
		}
		props = append(props, property)
	}
	rows.Close()
	return props, nil
}

func (p *Property) DeletProperty() error {
	if err := postgres.Db.QueryRow("DELETE from property WHERE Pid = $1", p.Pid).Scan(&p.Pid); err != nil {
		return err
	}
	return nil
}

func (p *Property) SearchEngine(sitem string) ([]Property, error) {
	rows, err := postgres.Db.Query("SELECT * FROM property WHERE Plocation ILIKE $1 OR Pcost ILIKE $1 OR Pname ILIKE $1 OR Pdes ILIKE $1 OR Ptype ILIKE $1", "%"+sitem+"%")

	if err != nil {
		return nil, err
	}

	properties := []Property{}

	for rows.Next() {
		property := Property{}

		err := rows.Scan(&property.Pid, &property.Pname, &property.Ptype, &property.Plocation, &property.Pdes, &property.Pcost, &property.OE, &property.Pimg)

		if err != nil {
			return nil, err
		}
		properties = append(properties, property)
	}
	rows.Close()
	return properties, nil
}
