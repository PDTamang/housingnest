package model

import postgres "hn/datastore"

type Web struct {
	Wid    string
	Writer string
	Sgiven int
	Rgiven string
}

func (srl *Web) GiveWeb() error {
	_, err := postgres.Db.Exec("INSERT INTO webside(Wid, Writer, Rgiven, Sgiven) VALUES($1,$2,$3,$4)", srl.Wid, srl.Writer, srl.Rgiven, srl.Sgiven)
	return err
}
func GetWeb() ([]Web, error) {
	rows, getErr := postgres.Db.Query("Select * from webside")

	if getErr != nil {
		return nil, getErr
	}

	srl := []Web{}

	for rows.Next() {
		sr := Web{}
		dbErr := rows.Scan(&sr.Wid, &sr.Writer, &sr.Rgiven, &sr.Sgiven)

		if dbErr != nil {
			return nil, dbErr
		}
		srl = append(srl, sr)
	}
	rows.Close()
	return srl, nil
}
