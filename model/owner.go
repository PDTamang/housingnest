package model

import (
	"fmt"
	postgres "hn/datastore"
)

type User struct {
	ProImg    string `json:"proimg"`
	FullName  string `json:"fname"`
	CIDnumber string
	PhoneNo   string
	Email     string `json:"email"`
	Password  string `json:"pw"`
	Fb        string
	Whatsapp  string
	Tg        string
	Aemail    string
}

const (
	queryCreateUser         = "INSERT INTO users (proimg, fname, cidno, pno ,email, pw, fb, whatsapp, tg, aemail) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);"
	queryCheck              = "SELECT * FROM users WHERE Email = $1;"
	queryGetSignup          = "SELECT * FROM users WHERE Email =$1"
	queryDeleteOwner        = "DELETE from users WHERE Email = $1"
	queryUpdateOwnerProfile = "UPDATE users set proimg = $1, fname = $2, pno = $3, fb = $4, whatsapp = $5, tg = $6 WHERE email = $7 RETURNING email "
)

func (u *User) CreateUser(a []byte) error {
	_, err := postgres.Db.Exec(queryCreateUser, u.ProImg, u.FullName, u.CIDnumber, u.PhoneNo, u.Email, a, u.Fb, u.Whatsapp, u.Tg, u.Aemail)
	fmt.Println(err)
	return err
}

func (u *User) CheckUser() ([]byte, error) {
	var dbHash []byte
	err := postgres.Db.QueryRow(queryCheck, u.Email).Scan(&u.ProImg, &u.FullName, &u.CIDnumber, &u.PhoneNo, &u.Email, &dbHash, &u.Fb, &u.Whatsapp, &u.Tg, &u.Aemail)
	return dbHash, err
}

func (u *User) GetSignup() error {
	err := postgres.Db.QueryRow(queryGetSignup, u.Email).Scan(&u.ProImg, &u.FullName, &u.CIDnumber, &u.PhoneNo, &u.Email, &u.Password, &u.Fb, &u.Whatsapp, &u.Tg, &u.Aemail)
	return err
}

func GetAllOwners() ([]User, error) {
	const queryGetAll = "SELECT * FROM users;"
	table, err := postgres.Db.Query(queryGetAll)
	if err != nil {
		return nil, err
	}
	onwers := []User{}
	for table.Next() {
		var u User
		dbErr := table.Scan(&u.ProImg, &u.FullName, &u.CIDnumber, &u.PhoneNo, &u.Email, &u.Password, &u.Fb, &u.Whatsapp, &u.Tg, &u.Aemail)
		if dbErr != nil {
			return nil, dbErr
		}
		onwers = append(onwers, u)
	}
	table.Close()
	return onwers, nil
}
func (u *User) DeleteOwner() error {
	if err := postgres.Db.QueryRow(queryDeleteOwner, u.Email).Scan(&u.Email); err != nil {
		return err
	}
	return nil
}
func (u *User) UpdateOwnerProfile(email string) error {
	err := postgres.Db.QueryRow(queryUpdateOwnerProfile, u.ProImg, u.FullName, u.PhoneNo, u.Fb, u.Whatsapp, u.Tg, email).Scan(&u.Email)
	return err
}
