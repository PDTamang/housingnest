package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"hn/httpResp"
	"hn/model"
	"net/http"

	"github.com/gorilla/mux"
)

func SearchEngineHandler(w http.ResponseWriter, r *http.Request) {
	search := mux.Vars(r)["search"]
	var property model.Property
	props, dbErr := property.SearchEngine(search)

	if dbErr != nil {
		switch dbErr {
		case sql.ErrNoRows:
			fmt.Println(dbErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, "No such files")
			return
		default:
			fmt.Println(dbErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
			return
		}
	}
	fmt.Println(props)
	httpResp.RespondWithJson(w, http.StatusCreated, props)
}

func CreatePropertyHandler(w http.ResponseWriter, r *http.Request) {

	//  fmt.Fprintf(w,"add property handler ")
	var prop model.Property

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&prop)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}

	dbErr := prop.CreateProperty()
	if dbErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		// w.Write([]byte(dbErr.Error()))
		return
	} else {
		httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "property data added successfully"})

	}
}

func UpdatePropertyHandler(w http.ResponseWriter, r *http.Request) {
	pid := mux.Vars(r)["pid"]
	var prop model.Property
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&prop)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updateTitleErr := prop.UpdatePropertyDetails(pid)
	if updateTitleErr != nil {
		switch updateTitleErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "property not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateTitleErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, prop)
	}
}

func GetPersonalPropertiesHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered")
	email := mux.Vars(r)["email"]
	props := model.Property{OE: email}
	allprops, err := props.GetAllPersonalProps()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println(err.Error())
		return
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, allprops)
		fmt.Println("success")
	}
}

func GetAllHouseHandler(w http.ResponseWriter, r *http.Request) {
	books, getErr := model.GetAllHouse()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, books)
}
func GetAllGhouseHandler(w http.ResponseWriter, r *http.Request) {
	books, getErr := model.GetAllGhouse()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, books)
}
func GetAllVillaHandler(w http.ResponseWriter, r *http.Request) {
	books, getErr := model.GetAllVilla()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, books)
}

func GetPropertyHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered")
	pid := mux.Vars(r)["Pid"]

	props := model.Property{Pid: pid}

	getErr := props.GetProperty()
	fmt.Println(pid)
	fmt.Println(props)

	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			fmt.Println("popos sent not found ")
			httpResp.RespondWithError(w, http.StatusNotFound, "prop sent not found")
		default:
			fmt.Println(getErr)
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		fmt.Println("success")
		httpResp.RespondWithJson(w, http.StatusOK, props)
	}
}

func DeletePropertyHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered DeletProperty handler")
	pid := mux.Vars(r)["pid"]
	prop := model.Property{Pid: pid}
	fmt.Println(pid)

	dbErr := prop.DeletProperty()
	if dbErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		fmt.Println(dbErr)
		return
	}
	fmt.Println("DELETE")
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}
