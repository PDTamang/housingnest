package controller

import (
	"encoding/json"
	"fmt"
	"hn/httpResp"
	"hn/model"
	"net/http"
)

func GiveWebHandler(w http.ResponseWriter, r *http.Request) {

	//  fmt.Fprintf(w,"add weberty handler ")
	var web model.Web
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&web)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}

	dbErr := web.GiveWeb()
	if dbErr != nil {
		fmt.Println(dbErr)
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		// w.Write([]byte(dbErr.Error()))
		return
	} else {
		fmt.Println("SR given")
		httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "SR data added successfully"})

	}
}
func GetWebLHandler(w http.ResponseWriter, r *http.Request) {
	web, getErr := model.GetWeb()

	if getErr != nil {
		fmt.Println(getErr)
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	fmt.Println(web)
	httpResp.RespondWithJson(w, http.StatusCreated, web)
}
