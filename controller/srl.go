package controller

import (
	"encoding/json"
	"fmt"
	"hn/httpResp"
	"hn/model"
	"net/http"

	"github.com/gorilla/mux"
)

func GiveSRHandler(w http.ResponseWriter, r *http.Request) {

	//  fmt.Fprintf(w,"add srerty handler ")
	var sr model.SRL
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&sr)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}

	dbErr := sr.GiveSR()
	if dbErr != nil {
		fmt.Println(dbErr)
		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
		// w.Write([]byte(dbErr.Error()))
		return
	} else {
		fmt.Println("SR given")
		httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "SR data added successfully"})

	}
}
func GetAllSRLHandler(w http.ResponseWriter, r *http.Request) {
	sr, getErr := model.GetAllSRL()

	if getErr != nil {
		fmt.Println(getErr)
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	fmt.Println(sr)
	httpResp.RespondWithJson(w, http.StatusCreated, sr)
}

func GetSrlForoneHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered")
	pid := mux.Vars(r)["pid"]
	rs := model.SRL{Pid: pid}
	allrs, err := rs.GetSrlforone()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println(err.Error())
		return
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, allrs)
		fmt.Println("success")
	}
}
