package controller

// import (
// 	"encoding/json"
// 	"fmt"
// 	"hn/httpResp"
// 	"hn/model"
// 	"net/http"
// )

// func CreateImageHandler(w http.ResponseWriter, r *http.Request) {
// 	var images []model.Image
// 	err := json.NewDecoder(r.Body).Decode(&images)
// 	if err != nil {
// 		fmt.Println("Could not decode")
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "Could not decode")
// 		return
// 	}
// 	fmt.Println(images[0].Pid)

// 	dbErr := model.CreateImages(images)
// 	if dbErr != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
// 		fmt.Println(dbErr)
// 		return
// 	}

// 	fmt.Println("Images added successfully")
// 	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "Image data added successfully"})
// }

// func UpdateImageHandler(w http.ResponseWriter, r *http.Request) {
// 	var images []model.Image
// 	err := json.NewDecoder(r.Body).Decode(&images)
// 	if err != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "Could not decode")
// 		return
// 	}

// 	dbErr := model.UpdateImages(images)
// 	if dbErr != nil {
// 		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
// 		return
// 	}

// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "Image data updated successfully"})
// }
