package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"hn/httpResp"
	"hn/model"
	"net/http"

	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func CreateAdminHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered signup")
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "couldn't decode the request")
		fmt.Println("error in decoding the request", err)
		return
	}
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(admin.Apw), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:", err)
		return
	}
	fmt.Println("hash:", hash)
	fmt.Println("string(hash)", string(hash))
	addErr := admin.CreateAdmin(hash)

	if addErr != nil {
		if e, ok := addErr.(*pq.Error); ok {
			if e.Code == "23505" {
				fmt.Print("duplicate key error upper")
				httpResp.RespondWithError(w, http.StatusNotAcceptable, "duplicate key error")
				return
			}
		} else {
			fmt.Println("error in inserting the data upper")
			httpResp.RespondWithError(w, http.StatusBadRequest, "error in inserting the data")
			return
		}
	}
	if addErr != nil {
		fmt.Print("error in inserting the data", addErr)
		httpResp.RespondWithError(w, http.StatusBadRequest, "error in adding the data")
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "added successfully"})
	fmt.Println("successful")

}

func LoginAdminHandler(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	var err = json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println("error in decoding the request")
		return
	}
	dbHash, checkErr := admin.Check()
	if checkErr != nil {
		switch checkErr {
		case sql.ErrNoRows:
			fmt.Print("invalid login norwo")
			httpResp.RespondWithError(w, http.StatusNotFound, "Invalid row")

		default:
			fmt.Print("error in getting the data to compare", checkErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, "error with the database e")
		}
		return
	}
	pwdErr := bcrypt.CompareHashAndPassword([]byte(dbHash), []byte(admin.Apw))
	if pwdErr != nil {
		fmt.Println("invalid with password")
		httpResp.RespondWithError(w, http.StatusNotFound, "Invalid login, try again password")
		return
	}
	fmt.Println("login successful")
	httpResp.RespondWithJson(w, http.StatusAccepted, map[string]string{"message": "successful"})
}
