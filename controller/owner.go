package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"hn/httpResp"
	"hn/model"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func SignupHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered signup")
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "couldn't decode the request")
		fmt.Println("error in decoding the request", err)
		return
	}
	var hash []byte
	hash, err = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("bcrypt err:", err)
		return
	}
	fmt.Println("hash:", hash)
	fmt.Println("string(hash)", string(hash))
	user.Aemail = "admin@gmail.com"

	addErr := user.CreateUser(hash)

	if addErr != nil {
		if e, ok := addErr.(*pq.Error); ok {
			if e.Code == "23505" {
				fmt.Print("duplicate key error")
				httpResp.RespondWithError(w, http.StatusNotAcceptable, "duplicate key error")
				return
			}
		} else {
			fmt.Println("error in inserting the data")
			httpResp.RespondWithError(w, http.StatusBadRequest, "error in inserting the data")
			return
		}
	}

	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"message": "added successfully"})
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println("error in decoding the request")
		return
	}
	dbHash, checkErr := user.CheckUser()
	if checkErr != nil {
		switch checkErr {
		case sql.ErrNoRows:
			fmt.Println("invalid login no row")
			httpResp.RespondWithError(w, http.StatusNotFound, "Invalid login, try again no rowkjlaskjflkj")
			return
		default:
			fmt.Println("error in getting the data to compare", checkErr)
			httpResp.RespondWithError(w, http.StatusBadRequest, "error with the database")
			return
		}
	}
	pwdErr := bcrypt.CompareHashAndPassword([]byte(dbHash), []byte(user.Password))
	if pwdErr != nil {
		fmt.Println("invalid with password")
		httpResp.RespondWithError(w, http.StatusNotFound, "Invalid login, try again password")
		return
	}
	fmt.Println("login successful")
	httpResp.RespondWithJson(w, http.StatusAccepted, map[string]string{"message": "successful"})
}
func GetSignupHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	user := model.User{Email: email}
	getErr := user.GetSignup()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			fmt.Println("user not found")
			httpResp.RespondWithError(w, http.StatusNotFound, "user not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, user)
	}
}

func GetAllOwnerHandler(w http.ResponseWriter, r *http.Request) {
	onwers, getErr := model.GetAllOwners()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, onwers)
}

func DeleteOwnerHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered DeletOwner handler")
	email := mux.Vars(r)["email"]
	prop := model.User{Email: email}
	fmt.Println(email)
	err := prop.DeleteOwner()
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println(err)
		return
	}
	fmt.Println("DELETE")
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func UpdateOwenerHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var owner model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&owner)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updateTitleErr := owner.UpdateOwnerProfile(email)
	if updateTitleErr != nil {
		switch updateTitleErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "owner not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateTitleErr.Error())
		}
	} else {
		httpResp.RespondWithJson(w, http.StatusOK, owner)
	}
}

// func LogoutHandler(w http.ResponseWriter, r *http.Request) {
// 	http.SetCookie(w, &http.Cookie{
// 		Name:    "my-cookie",
// 		Expires: time.Now(),
// 	})
// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "logout successful"})
// }
