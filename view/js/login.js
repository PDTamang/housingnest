function toOwnerPage() {
    var data = {
        email: document.getElementById("email").value,
        pw: document.getElementById("pw").value
    }

    // alert(data.email)

    fetch("/login",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{"content-type":"application/json"}})
        .then(response =>{
        if (response.status == 202){
            localStorage.setItem('email', document.getElementById("email").value);
            location.assign('/indexowner.html')
        }
        else if (response.status == 404){
            alert("Invalid login, try again")
        }else{
            throw new Error(response.statusText)
        }
    }).catch(e => {
        alert(e)
    })
}
function toAdminPage() {
    var data = {
        aemail: document.getElementById("email").value,
        apw: document.getElementById("password").value
    };

    fetch("/adminLogin", {
        method: "POST",
        body: JSON.stringify(data),
        headers: { "Content-Type": "application/json; charset=utf-8" }
    }).then(response =>{
        if (response.status == 202){
            
            window.location.href = "indexadmin.html"
        }
        else if (response.status == 404){
            alert("Invalid login, try again lll")
        }else{
            throw new Error(response.statusText)
        }
    }).catch(e => {
        alert(e)
    })
}

