// const uploadInput = document.getElementById('image-upload');
// const imageRow = document.querySelector('.image-row');

// uploadInput.addEventListener('change', (event) => {
//   const file = event.target.files[0];
//   const reader = new FileReader();
//   reader.readAsDataURL(file);
//   reader.onload = () => {
//     const newImage = document.createElement('img');
//     newImage.style.width = "23%";
//     newImage.style.height = "10rem";
//     newImage.style.margin= "1rem";

//     newImage.src = reader.result;
//     imageRow.insertBefore(newImage, uploadInput.parentElement);
//   };
// });




// const uploadInput = document.getElementById('image-upload');

// uploadInput.addEventListener('change', (event) => {
//   const file = event.target.files[0];
//   const reader = new FileReader();
//   reader.readAsDataURL(file);
//   reader.onload = () => {
//     const imageData = reader.result;

//     // Send the image data to the backend API
//     fetch('/upload', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//       },
//       body: JSON.stringify(imageData),
//     })
//       .then((response) => response.json())
//       .then((data) => {
//         // Handle the response from the backend, if needed
//         console.log(data);
//       })
//       .catch(e => {
//         alert(e)
//       });
//   };
// });


const uploadInput = document.getElementById('image-upload');

uploadInput.addEventListener('change', (event) => {
  const file = event.target.files[0];
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {
    const imageData = reader.result;

    // Create an object with the imageData property
    const dataToSend = {
      img: imageData,
    };

    // Send the image data to the backend API
    fetch('/upload', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(dataToSend),
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response from the backend, if needed
        console.log(data);
      })
      .catch(e => {
        alert(e)
      });
  };
});






