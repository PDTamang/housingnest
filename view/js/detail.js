const email = localStorage.getItem('email');
if (!email) {
    alert("Please login to visit the pages")
    window.location.href = 'loginowner.html';
  }

window.onload = function() {
    if (!email) {
        alert("Please login to visit the pages")
        window.location.href = 'index.html';
      }
    
    fetch('/signup/' + email)
    .then(response => response.text())
    .then(data => replaceName(data))
}

function replaceName(data) {
    detail = JSON.parse(data)
    console.log(detail)
    document.querySelector('.profile-icon').src = detail.proimg
    document.querySelector('.image5').src = detail.proimg
    document.querySelector('.owner-name').innerHTML = detail.fname;
    document.querySelector('.contact-no-pro > span').innerHTML = detail.PhoneNo;
    document.querySelector('a.tg').href = detail.Tg
    document.querySelector('a.wt').href = detail.Whatsapp
    document.querySelector('a.fb').href = detail.Fb
}

document.querySelector("#Propicture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl2", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})

function updataProfile() {
    // alert(localStorage.getItem("image-dataurl2"))

    const newownerinfo = {
        proimg: localStorage.getItem("image-dataurl2"),
        fname: document.querySelector("#name").value,
        PhoneNo: document.querySelector("#pno").value,
        Whatsapp: document.querySelector("#whatsapp-link").value,
        Tg: document.querySelector("#telegram-link").value,
        Fb: document.querySelector("#facebook-link").value,

     }


    fetch("/user/"+email,{
        method: "PUT",
        body:JSON.stringify(newownerinfo),
        headers: {"Content-type":"application/json; charset=UTF-8"}
    }
    ).then(response => response.text())
    .then(newdata => replaceName(newdata))
    .catch(e => {
        alert(e)
    })
}

