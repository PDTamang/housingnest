const OE = localStorage.getItem('email');



document.addEventListener('DOMContentLoaded', function () {
  ;
  fetch("/ownerPros/" + OE)
    .then((response) => response.text())
    .then((data) => GetAllMyProperty(data));
});

async function GetAllMyProperty(data) {

  const props = JSON.parse(data);
  console.log(data);
  props.forEach((prop) => {
    console.log(prop);
    var megaContainer = document.querySelector(".maga-Container");
    var getMyProps = getMyPropsfunction(prop);
    megaContainer.appendChild(getMyProps);
  });
}


function getMyPropsfunction(prop) {
  var all = document.createElement("div");
  all.classList.add("all");

  var cardPro = document.createElement("div");
  cardPro.classList.add("card-pro");

  var proImg = document.createElement("div");
  proImg.classList.add("pro-img");

  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");
  var proImgImg = document.createElement("img");
  proImgImg.classList.add("pro-img");
  proImgImg.setAttribute("src", prop.Pimg);
  proImgImg.onload = function () {
    var imgWidth = proImgImg.width;
    var imgHeight = proImgImg.height;
    canvas.width = imgWidth;
    canvas.height = imgHeight;
    ctx.drawImage(proImgImg, 0, 0, imgWidth, imgHeight);
  };
  proImg.appendChild(proImgImg);
  cardPro.appendChild(proImg);

  var proBody = document.createElement("div");
  proBody.classList.add("pro-body");

  var tables = document.createElement("table");
  tables.classList.add("btns");

  var trs = document.createElement("tr");
  var tds = document.createElement("td");
  var tdDives = document.createElement("div");
  tdDives.classList.add("div-edit-btn", "div-pro-btn");
  var ainner = document.createElement("a");
  ainner.classList.add("edit-btn", "pro-btn");
  ainner.onclick = function(){
    localStorage.setItem("propid", prop.Pid)
    
    window.location.href ="updataProp.html"
   
  }
  ainner.innerHTML = "Edit";

  tdDives.appendChild(ainner);
  tds.appendChild(tdDives);
  trs.appendChild(tds);

  var stds = document.createElement("td");
  var stdDives = document.createElement("div");
  stdDives.classList.add("div-delete-btn", "div-pro-btn");
  var sainner = document.createElement("a");
  sainner.classList.add("delete-btn", "pro-btn");
  sainner.innerHTML = "Delete";
  stdDives.onclick = function() {
    deleteProperty(prop.Pid); // Call deleteProperty function with property ID
  };

  stdDives.appendChild(sainner);
  stds.appendChild(stdDives);
  trs.appendChild(stds);
  tables.appendChild(trs);
  proBody.appendChild(tables);

  var proDis = document.createElement("div");
  proDis.classList.add("pro-dis");

  var hprice = document.createElement("p");
  hprice.classList.add("h-name");
  hprice.innerHTML = prop.Pname;
  proDis.appendChild(hprice);

  var hlocatio = document.createElement("p");
  hlocatio.classList.add("h-location");
  hlocatio.innerHTML = "Location: " + prop.Plocation;
  proDis.appendChild(hlocatio);

  var hprice = document.createElement("p");
  hprice.classList.add("h-price");
  hprice.innerHTML = "Monthly Rent: " + prop.Pcost + "/Month";
  proDis.appendChild(hprice);

  proBody.appendChild(proDis);
  cardPro.appendChild(proBody);
  all.appendChild(cardPro);

  var longLine = document.createElement("img")
  longLine.classList.add("footer-line")
  longLine.src = "images/remove.png"
  longLine.style.marginBottom = "2rem";
  var pid = document.createElement("p")
  pid.innerHTML = prop.Pid
  pid.style.visibility = "hidden";//// id hidden



  var lilAll = document.createElement("div")
  lilAll.appendChild(longLine)
  lilAll.appendChild(pid)
  lilAll.appendChild(all)

  return lilAll

}


function deleteProperty(pid) {
  fetch("/deletepros/" + pid, {
    method: "DELETE"
  })
    .then((response) => {
      if (response.ok) {
        console.log("Property deleted successfully");
        // Perform any additional actions or UI updates
           parentElement.remove();
        location.reload();

      } else {
        location.reload();
        parentElement.remove();

        console.error("Failed to delete property");
        
      }
    })
    .catch((error) => {
      location.reload();
      // parentElement.remove();
      console.error("Error deleting property:", error);
    });
}
