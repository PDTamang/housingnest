const searchIcon = document.querySelector('.search-icon');
const inputbox = document.querySelector('.inputSearch');
const searchbar = document.querySelector('.input');

searchIcon.addEventListener('click', () => {
  // alert("hi")
    let s = {
        search: inputbox.value
    } 
    console.log(s.search)
    // window.location.href = 'search.html';


    fetch("/search/" + s.search, {
        method: "POST",
        body: JSON.stringify(s),
        headers: {"content-type":"application/json; charset-utf 8"}
    })
    .then(resp => resp.text())
    .then(props => showSearchedProperty(props))
    }).catch(e =>{
        alert(e)
    })

   

async function showSearchedProperty(data) {
    // alert("200")
    const books = JSON.parse(data);
    console.log(books)
    books.forEach((book) => {
      console.log(book)



      var megaContainer =  document.querySelector(".searchItems > .contents");

      var card = document.createElement("div")
      card.classList.add("card")
        
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      var proimg = document.createElement("img")
      proimg.id = "imgs"
      proimg.setAttribute("src", book.Pimg)
      proimg.onload = function () {
        var imgWidth = image.width;
        var imgHeight = image.height;
        canvas.width = imgWidth;
        canvas.height = imgHeight;
        ctx.drawImage(image, 0, 0, imgWidth, imgHeight);
      };
      card.appendChild(proimg)

      var cardContent = document.createElement("div")
      cardContent.classList.add("card-content")

      var unlike = document.createElement("img")
      unlike.classList.add("plain-heart", "heart")
      unlike.src = "images/plain-heart.png"
      cardContent.appendChild(unlike)

      var counting = document.createElement("p")
      counting.classList.add("countings")
      counting.innerHTML = "12000"
      cardContent.appendChild(counting)

      var pname = document.createElement("p")
      pname.classList.add("name")
      pname.innerHTML = book.Pname
      cardContent.appendChild(pname)
      
      var plocation = document.createElement("p")
      plocation.classList.add("name")
      plocation.innerHTML = book.Plocation
      cardContent.appendChild(plocation)

      var ptype = document.createElement("p")
      ptype.classList.add("name")
      ptype.textContent = "House"
      cardContent.appendChild(ptype)

      var btns = document.createElement("div")
      btns.classList.add("view-button", "bttns", "button")

      var viewMoreBtn = document.createElement("a");
      viewMoreBtn.classList.add("view-link-btn");
      viewMoreBtn.href = "viewproduct.html";
      viewMoreBtn.textContent = "View More";
      
      btns.appendChild(viewMoreBtn)
      cardContent.appendChild(btns)

      card.appendChild(cardContent)
      megaContainer.appendChild(card)
      inputbox.value = ''

    });
  }