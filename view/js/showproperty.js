window.onload = function () {
    fetch("/house")
      .then((response) => response.text())
      .then((data) => GetAllHouse(data))

    fetch("/ghouse")
      .then((response) => response.text())
      .then((data) => GetAllGuestHouse(data));

    fetch("/villa")
      .then((response) => response.text())
      .then((data) => GetAllVilla(data));
  };




  async function GetAllHouse(data) {
    const props = JSON.parse(data);
    console.log(data)
    props.forEach((prop) => {
      console.log(prop)
      var megaContainer = document.querySelector(".house > .contents");

      var getCards = getCardfunction(prop)
      

      megaContainer.appendChild(getCards)
    });
  }



    async function GetAllGuestHouse(data) {
      const props = JSON.parse(data);
      console.log(data)
      props.forEach((prop) => {
        console.log(prop)
        var megaContainer = document.querySelector(".ghouse > .contents");
  
        var getCards = getCardfunction(prop)
        // console(getCards, 2)
  
        megaContainer.appendChild(getCards)
      });
    }
      async function GetAllVilla(data) {

        const props = JSON.parse(data);
        console.log(data)
        props.forEach((prop) => {
          console.log(prop)
          var megaContainer = document.querySelector(".villa > .contents");
    
          var getCards = getCardfunction(prop)
          
    
          megaContainer.appendChild(getCards)
        });
      }
function getCardfunction(prop) {
  var card = document.createElement("div")
  card.classList.add("card")
  
    
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");
  var proimg = document.createElement("img")
  proimg.id = "imgs"
  proimg.setAttribute("src", prop.Pimg)
  proimg.onload = function () {
    var imgWidth = image.width;
    var imgHeight = image.height;
    canvas.width = imgWidth;
    canvas.height = imgHeight;
    ctx.drawImage(image, 0, 0, imgWidth, imgHeight);
  };
  card.appendChild(proimg)

  var cardContent = document.createElement("div")
  cardContent.classList.add("card-content")

  var unlike = document.createElement("img")
  unlike.classList.add("plain-heart", "heart")
  unlike.src = "images/plain-heart.png"
  cardContent.appendChild(unlike)

  var counting = document.createElement("p")
  counting.classList.add("countings")
  counting.innerHTML = "12000"
  cardContent.appendChild(counting)

  var pname = document.createElement("p")
  pname.classList.add("name")
  pname.innerHTML = prop.Pname
  cardContent.appendChild(pname)
  
  var plocation = document.createElement("p")
  plocation.classList.add("name")
  plocation.innerHTML = prop.Plocation
  cardContent.appendChild(plocation)

  var ptype = document.createElement("p")
  ptype.classList.add("name")
  ptype.textContent = prop.Ptype
  cardContent.appendChild(ptype)

  var btns = document.createElement("div")
  btns.classList.add("view-button", "bttns", "button")

  var viewMoreBtn = document.createElement("a");
  viewMoreBtn.classList.add("view-link-btn");
  viewMoreBtn.onclick = function() {
    localStorage.setItem("Pid", prop.Pid)
    window.location.href = "viewproduct.html"
        
  };;
  viewMoreBtn.textContent = "View More";
  
  btns.appendChild(viewMoreBtn)
  cardContent.appendChild(btns)
  // var pid = document.createElement("p") /// hide id
  // pid.innerHTML = prop.Pid
  // pid.style.visibility = "hidden";
  
  // console.log(prop.Pid)
  // cardContent.appendChild(pid)
  card.appendChild(cardContent)

  
  return card
}