const pid = localStorage.getItem("propid")
const email = localStorage.getItem("email")

window.onload = function() {
    fetch("/oneproperty/" + pid)
    .then((response) => response.text())
    .then((data) => GetProperty(data));
}

function GetProperty(data) {
    var propdetail = JSON.parse(data)
    document.getElementById("ISBN").value = propdetail.Pname;
    document.getElementById("bookTitle").value = propdetail.Plocation;
    document.getElementById("bookDesc").value = propdetail.Pdes;
    document.getElementById("price").value = propdetail.Pcost;
}


const uploadForm = document.getElementById("uploadForm");
var input = document.getElementById("bookImage");

input.addEventListener("change", handleFiles, false);

function handleFiles(e) {
  var canvas = document.createElement("canvas");
  var ctx = canvas.getContext("2d");

  var img = new Image();

  img.onload = function () {
    var imgWidth = img.width;
    var imgHeight = img.height;
    canvas.width = imgWidth;
    canvas.height = imgHeight;
    ctx.drawImage(img, 0, 0, imgWidth, imgHeight);

    var base64 = canvas.toDataURL();
    document.getElementById("urlholder").value = base64;
  };

  img.src = URL.createObjectURL(e.target.files[0]);
}

uploadForm.addEventListener("submit", (e) => {
  e.preventDefault();
  addBook();
});

async function addBook() {
  const  uniqueNumber = Date.now() + Math.floor(Math.random() *1000)
  var uniqueNumberString = uniqueNumber.toString();
  console.log(document.getElementById("urlholder").value);
   var selectedOption = document.getElementById("propertyType");
  var selectedText = selectedOption.options[selectedOption.selectedIndex].text;
  var _data = {
    // Pid: uniqueNumberString,
    Pname: document.getElementById("ISBN").value,
    Ptype: selectedText,
    Plocation: document.getElementById("bookTitle").value,
    Pdes: document.getElementById("bookDesc").value,
    Pcost: document.getElementById("price").value,
    // OE: email,
    Pimg: document.getElementById("urlholder").value,
  };

  console.log(_data);

  fetch("/property/"+pid, {
    method: "PUT",
    body: JSON.stringify(_data),
    headers: { "Content-type": "application/json; charset = UTF-8" },
  });

  resetForm();
}

function resetForm() {
  document.getElementById("ISBN").value = "";
  document.getElementById("bookTitle").value = "";
  document.getElementById("bookDesc").value = "";
  document.getElementById("price").value = "";
  document.getElementById("urlholder").value = "";
//   document.getElementById("ptype").value ="";
  document.getElementById("propertyType").value ="";
}

