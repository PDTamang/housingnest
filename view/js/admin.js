window.onload = function(){
    fetch('/signups')
    .then(response => response.text())
    .then(data => showOwners(data))    
}
function showOwners(data){
    const ow = JSON.parse(data)
    console.log(ow, "here")
    ow.forEach(ons => {
        showOwner(ons)
    });
}

const forms = document.getElementById("pop-form")
const table = document.querySelector(".owner")

function showForm() {
    forms.classList.remove("hidden")
}

function addOwner() {
   const ownerInfo = newOwnerInfo()
   console.log(ownerInfo)
    
    fetch("/signup",{
        method: "POST",
        body: JSON.stringify(ownerInfo),
        headers:{"content-type":"application/json; charset-utf 8"}
    })
    .then(response => {
        if(response.status == 201){
             showOwner(ownerInfo);
             forms.classList.add("hidden");
        }else if (response.status == 406){
            alert("The email is already registered with us, try logging in")
        }else{
        throw new Error(response.statusText)
        }
    }).catch(e =>{
        alert(e)
    })
    
}

document.querySelector("#picture").addEventListener("change", function(){
   
    const dataurl = new FileReader();
    dataurl.addEventListener("load", ()=>{
        localStorage.setItem("image-dataurl", dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})

function newOwnerInfo() {

    // alert(localStorage.getItem("image-dataurl"))

        const newOwner = {
            proimg: localStorage.getItem("image-dataurl"),
            fname: document.querySelector("#name").value,
            CIDnumber: document.querySelector("#cid").value,
            PhoneNo: document.querySelector("#pno").value,
            email: document.querySelector("#email").value,
            pw: document.querySelector("#password").value
    }
    
    console.log(newOwner.proimg)
    return newOwner
}

function showOwner(ownerInfo) {
    const owner = owners(ownerInfo)
    return owner
  
}

function owners(ownerInfo) {
    const newons = document.createElement('tr');
    const firstTdpic = document.createElement('td');
    const proImg = document.createElement('img');

    proImg.classList.add('pro-pic');
    proImg.setAttribute("src", ownerInfo.proimg)

    firstTdpic.appendChild(proImg);

    const nameTd = document.createElement('td');
    nameTd.innerHTML = ownerInfo.fname;

    const cid = document.createElement('td')
    cid.innerHTML = ownerInfo.CIDnumber

    const pno = document.createElement('td')
    pno.innerHTML = ownerInfo.PhoneNo

    const email = document.createElement('td');
    email.classList.add('email-cell');
    email.innerHTML = ownerInfo.email;
    const delbtns = document.createElement('td');
    const dells = document.createElement('div');
    dells.innerHTML = 'Delete';
    dells.onclick = deleteOwner;
    dells.classList.add("deletbtn");
    delbtns.appendChild(dells);

  

    newons.appendChild(firstTdpic);
    newons.appendChild(nameTd);
    newons.appendChild(cid);
    newons.appendChild(pno);
    newons.appendChild(email);
    newons.appendChild(delbtns);
    const table = document.getElementById('ow'); // Replace 'your-table-id' with the actual ID of your table
    table.appendChild(newons);

    return newons;
}


function deleteOwner() {
    if (confirm("Are you sure you want to delete this?")) {
        // Get the email value from the row
        const email = this.parentNode.parentNode.querySelector(".email-cell").innerHTML;
        console.log(email)
        // Send the email value to the URL parameter of the route
        fetch(`/signup/${encodeURIComponent(email)}`, {
            method: "DELETE"
        })
        .then(response => {
            if (response.status === 200) {
                alert("response delete")
                // If the deletion is successful, remove the row from the table
                const row = this.parentNode.parentNode;
                row.parentNode.removeChild(row);
            } else {
                throw new Error(response.statusText);
            }
        })
        .catch(error => {
            // alert("An error occurred while deleting the owner.");
            const row = this.parentNode.parentNode;
            row.parentNode.removeChild(row);
            console.error(error);
        });
    }
}



